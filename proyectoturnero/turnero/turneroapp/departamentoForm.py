from django import forms
from .models import Departamento 


class DepartamentoModelForm(forms.ModelForm):
    class Meta:
        model=Departamento
        fields=[ "nombre_departamento", "lugar_atencion","fecha_insercion","fecha_modificacion","activo"]

class DepartamentoFormulario(forms.Form):
    nombre_departamento = forms.CharField(max_length=8, required=True)
    lugar_atencion = forms.CharField(max_length=30)
    fecha_insercion = forms.DateTimeField()
    fecha_modificacion = forms.DateTimeField()
    activo = forms.BooleanField(required=True)