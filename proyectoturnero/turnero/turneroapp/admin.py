from django.contrib import admin
from . import models
from . personaForm import PersonaModelForm
from . estadoForm import EstadoModelForm
from . prioridadForm import PrioridadModelForm
from . departamentoForm import DepartamentoModelForm
from . tema_atencionForm import TemasAtencionModelForm

class PersonaAdmin(admin.ModelAdmin):
    list_display = ["ci_num","nombre","apellido","fecha_nacimiento","direccion","correo_electronico","celular_num","activo"]
    list_filter = ["ci_num","activo"]
    list_editable = ["activo","correo_electronico"]
    search_fields = ["ci_num"]
    form = PersonaModelForm

class EstadoAdmin(admin.ModelAdmin):
    list_display = ["descri_estado","activo"]
    list_filter = ["descri_estado","activo"]
    list_editable = ["activo"]
    search_fields = ["descri_estado"]
    form = EstadoModelForm

class PrioridadAdmin(admin.ModelAdmin):
    list_display = ["descri_prioridad","activo"]
    list_filter = ["descri_prioridad","activo"]
    list_editable = ["activo"]
    search_fields = ["descri_estado"]
    form = PrioridadModelForm

class DepartamentoAdmin(admin.ModelAdmin):
    list_display = ["nombre_departamento","activo"]
    list_filter = ["nombre_departamento","activo"]
    list_editable = ["activo"]
    search_fields = ["nombre_departamento"]
    form = DepartamentoModelForm

class TemasAtencionAdmin(admin.ModelAdmin):
    list_display = ["descri_tema_atencion","activo"]
    list_filter = ["descri_tema_atencion","activo"]
    list_editable = ["activo"]
    search_fields = ["descri_tema_atencion"]
    form = TemasAtencionModelForm


admin.site.register(models.Persona, PersonaAdmin)
admin.site.register(models.Estado, EstadoAdmin)
admin.site.register(models.Prioridad, PrioridadAdmin)
admin.site.register(models.Departamento, DepartamentoAdmin)
admin.site.register(models.TemasAtencion, TemasAtencionAdmin)