from django import forms
from .models import TemasAtencion 


class TemasAtencionModelForm(forms.ModelForm):
    class Meta:
        model=TemasAtencion
        fields=[ "descri_tema_atencion", "iniciales","fecha_insercion","fecha_modificacion","activo"]

class DepartamentoFormulario(forms.Form):
    descri_tema_atencion = forms.CharField(max_length=20, required=True)
    iniciales = forms.CharField(max_length=3)
    fecha_insercion = forms.DateTimeField()
    fecha_modificacion = forms.DateTimeField()
    activo = forms.BooleanField(required=True)