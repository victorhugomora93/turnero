from django.shortcuts import render

from .personaForm import PersonaFormulario
from .estadoForm import EstadoFormulario
from .prioridadForm import PrioridadFormulario
from .departamentoForm import DepartamentoFormulario
from .models import Persona
from .models import Estado
from .models import Prioridad
from .models import Departamento


def inicio(request):
    form = PersonaFormulario(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        print(form_data)
        aux1 = form_data.get("ci_num")
        aux2= form_data.get("nombre")
        aux3= form_data.get("apellido")
        aux4= form_data.get("fecha_nacimiento")
        aux5= form_data.get("direccion")
        aux6= form_data.get("correo_electronico")
        aux7= form_data.get("celular_num")
        aux8= form_data.get("fecha_insercion")
        aux9= form_data.get("fecha_modificacion")
        aux10=form_data.get("activo")
        try:
            obj =  Persona.objects.create(ci_num = aux1, nombre = aux2, apellido = aux3, fecha_nacimiento =  aux4, direccion = aux5, correo_electronico = aux6,
                                          celular_num =  aux7, fecha_insercion = aux8, fecha_modificacion = aux9, activo = aux10)
        except Exception as e:
            print("error", e)    
    context = {
        "titulo" : 'Formulario',
        "el_form": form
    }
    return render(request, "inicio.html", context)

def estado(request):
    form = EstadoFormulario(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        print(form_data)
        aux1= form_data.get("descri_estado")
        aux2= form_data.get("activo")
        aux3= form_data.get("fecha_insercion")
        aux4= form_data.get("fecha_modificacion")
        try:
            obj =  Estado.objects.create(descri_estado = aux1, activo = aux2, fecha_insercion = aux3, fecha_modificacion = aux4)
        except Exception as e:
            print("error", e)    
    context = {
        "titulo" : 'Formulario',
        "el_form": form
    }
    return render(request, "estado.html", context)


def prioridad(request):
    form = PrioridadFormulario(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        print(form_data)
        aux1= form_data.get("descri_prioridad")
        aux2= form_data.get("nivel")
        aux3= form_data.get("fecha_insercion")
        aux4= form_data.get("fecha_modificacion")
        aux5= form_data.get("activo")
        try:
            obj =  Prioridad.objects.create(descri_prioridad = aux1, nivel = aux2, fecha_insercion = aux3, fecha_modificacion = aux4, activo = aux5)
        except Exception as e:
            print("error", e)    
    context = {
        "titulo" : 'Formulario',
        "el_form": form
    }
    return render(request, "estado.html", context)

def departamento(request):
    form = DepartamentoFormulario(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        print(form_data)
        aux1= form_data.get("nombre_departamento")
        aux2= form_data.get("lugar_atencion")
        aux3= form_data.get("fecha_insercion")
        aux4= form_data.get("fecha_modificacion")
        aux5= form_data.get("activo")
        try:
            obj =  Prioridad.objects.create(nombre_departamento = aux1, lugar_atencion = aux2, fecha_insercion = aux3, fecha_modificacion = aux4, activo = aux5)
        except Exception as e:
            print("error", e)    
    context = {
        "titulo" : 'Formulario',
        "el_form": form
    }
    return render(request, "departamento.html", context)


