from django import forms
from .models import Estado

class EstadoModelForm(forms.ModelForm):
    class Meta:
        model=Estado
        fields=["descri_estado","activo","fecha_insercion","fecha_modificacion"]
    
class EstadoFormulario(forms.Form):
    descri_estado = forms.CharField(max_length=20, required=True)
    activo = forms.BooleanField(required=True)
    fecha_insercion = forms.DateTimeField()
    fecha_modificacion = forms.DateTimeField()