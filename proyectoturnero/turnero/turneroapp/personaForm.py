from django import forms
from .models import Persona 


class PersonaModelForm(forms.ModelForm):
    class Meta:
        model=Persona
        fields=["ci_num", "nombre", "apellido","fecha_nacimiento", "direccion","correo_electronico","celular_num","fecha_insercion","fecha_modificacion","activo"]

class PersonaFormulario(forms.Form):
    ci_num = forms.CharField(max_length=8, required=True)
    nombre = forms.CharField(max_length=30)
    apellido = forms.CharField(max_length=30)
    fecha_nacimiento = forms.DateField()
    direccion = forms.CharField(max_length=32)
    correo_electronico = forms.CharField(max_length=20)
    celular_num = forms.CharField(max_length=12)
    fecha_insercion = forms.DateTimeField()
    fecha_modificacion = forms.DateTimeField()
    activo = forms.BooleanField(required=True)
