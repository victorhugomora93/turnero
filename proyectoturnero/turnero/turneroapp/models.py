# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models
from django.utils import timezone




class Persona(models.Model):
    id = models.AutoField(primary_key=True)
    ci_num = models.CharField(max_length=8)
    nombre = models.CharField(max_length=30)
    apellido = models.CharField(max_length=30)
    fecha_nacimiento = models.DateField(blank=True, null=True)
    direccion = models.CharField(max_length=32, blank=True, null=True)
    correo_electronico = models.CharField(max_length=25, blank=True, null=True)
    celular_num = models.CharField(max_length=12, blank=True, null=True)
    fecha_insercion = models.DateTimeField(blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    activo = models.BooleanField(default=True)

    def __str__(self):
        return self.ci_num

    class Meta:
        db_table = 'persona'


class Cliente(models.Model):
    id = models.AutoField(primary_key=True)
    id_persona = models.ForeignKey('Persona', models.DO_NOTHING, db_column='id_persona')
    ruc = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        db_table = 'cliente'


class Departamento(models.Model):
    id = models.AutoField(primary_key=True)
    nombre_departamento = models.CharField(max_length=20)
    lugar_atencion = models.CharField(max_length=15)
    fecha_insercion =models.DateTimeField(blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    activo = models.BooleanField(default=True)

    class Meta:
        db_table = 'departamento'

class TemasAtencion(models.Model):
    id = models.AutoField(primary_key=True)
    descri_tema_atencion = models.CharField(max_length=20)
    iniciales = models.CharField(max_length=3)
    fecha_insercion = models.DateTimeField(blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    activo = models.BooleanField(default=True)

    class Meta:
        db_table = 'temas_atencion'


class DepartamentoAt(models.Model):
    id_departamento = models.OneToOneField(Departamento, models.DO_NOTHING, db_column='id_departamento', primary_key=True)
    id_tema_atencion = models.ForeignKey('TemasAtencion', models.DO_NOTHING, db_column='id_tema_atencion')

    class Meta:
        db_table = 'departamento_at'
        unique_together = (('id_departamento', 'id_tema_atencion'),)


class Empleado(models.Model):
    id = models.AutoField(primary_key=True)
    id_persona = models.ForeignKey('Persona', models.DO_NOTHING, db_column='id_persona')
    id_departamento = models.ForeignKey(Departamento, models.DO_NOTHING, db_column='id_departamento')
    fecha_insercion = models.DateTimeField(blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    activo = models.BooleanField(default=True)

    class Meta:
        db_table = 'empleado'


class Estado(models.Model):
    id = models.AutoField(primary_key=True)
    descri_estado = models.CharField(max_length=20)
    fecha_insercion = models.DateTimeField(blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    activo = models.BooleanField(default=True)

    class Meta:
        db_table = 'estado'


class Prioridad(models.Model):
    id = models.AutoField(primary_key=True)
    descri_prioridad = models.CharField(max_length=20)
    nivel = models.CharField(max_length=30)
    fecha_insercion =models.DateTimeField(blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    activo = models.BooleanField(default=True)

    class Meta:
        db_table = 'prioridad'


class PuestoAtencion(models.Model):
    id = models.AutoField(primary_key=True)
    nombre_puesto = models.CharField(max_length=30)

    class Meta:
        db_table = 'puesto_atencion'


class Rol(models.Model):
    id = models.AutoField(primary_key=True)
    codigo = models.CharField(max_length=8)
    descri_rol = models.CharField(max_length=20)
    activo = models.BooleanField(default=True)
    fecha_insercion =models.DateTimeField(blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'rol'


class Usuario(models.Model):
    id = models.AutoField(primary_key=True)
    id_empleado = models.ForeignKey(Empleado, models.DO_NOTHING, db_column='id_empleado')
    nombre_usuario = models.CharField(max_length=20)
    clave = models.CharField(max_length=20)
    fecha_insercion = models.DateTimeField(blank=True, null=True)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    activo = models.BooleanField(default=True)
    id_puesto_atencion = models.ForeignKey(PuestoAtencion, models.DO_NOTHING, db_column='id_puesto_atencion')

    class Meta:
        db_table = 'usuario'

class Turno(models.Model):
    id = models.AutoField(primary_key=True)
    fecha_ct = models.DateField()
    hora_ct = models.TimeField()
    fecha_atencion = models.DateField(blank=True, null=True)
    hora_atencion = models.TimeField(blank=True, null=True)
    hora_finalizacion = models.TimeField(blank=True, null=True)
    fecha_finalizacion = models.DateField(blank=True, null=True)
    duracion_espera = models.TimeField(blank=True, null=True)
    duracion_atencion = models.TimeField(blank=True, null=True)
    id_cliente = models.ForeignKey(Cliente, models.DO_NOTHING, db_column='id_cliente')
    id_tema_atencion = models.ForeignKey(TemasAtencion, models.DO_NOTHING, db_column='id_tema_atencion')
    id_prioridad = models.ForeignKey(Prioridad, models.DO_NOTHING, db_column='id_prioridad')
    id_estado = models.ForeignKey(Estado, models.DO_NOTHING, db_column='id_estado')
    id_usuario = models.ForeignKey('Usuario', models.DO_NOTHING, db_column='id_usuario', blank=True, null=True)

    class Meta:
        db_table = 'turno'


class UsuarioRol(models.Model):
    id_usuario = models.OneToOneField(Usuario, models.DO_NOTHING, db_column='id_usuario', primary_key=True)
    id_rol = models.ForeignKey(Rol, models.DO_NOTHING, db_column='id_rol')

    class Meta:
        db_table = 'usuario_rol'
        unique_together = (('id_usuario', 'id_rol'),)
