from django import forms
from .models import Prioridad

class PrioridadModelForm(forms.ModelForm):
    class Meta:
        model=Prioridad
        fields=["descri_prioridad","nivel","fecha_insercion","fecha_modificacion", "activo"]
    
class PrioridadFormulario(forms.Form):
    descri_prioridad = forms.CharField(max_length=20, required=True)
    nivel = forms.BooleanField(required=True)
    fecha_insercion = forms.DateTimeField()
    fecha_modificacion = forms.DateTimeField()
    activo = forms.BooleanField(required=True)